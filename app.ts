class Client {
	
	name:string;
	age:number;
	email:string;
	cpf:string;
	underage:boolean;

	constructor
	(name:string , age:number, email:string , cpf:string , underage:boolean) {
		this.name = name;
		this.age = age;
		this.email = email;
		this.cpf = cpf;
		this.underage = underage;
	}

	isUnderage() {
		if(this.underage) {
			return true;
		}
		else {
			return false;
		}
	}
}



class Pedido {
	
	id:number;
	client : Client;
	
	constructor(id:number , client:Client) {
		this.id = id;
		this.client = client;
	}
}

class Make {
	
	type:string;
	
	constructor(type:string) {
		this.type = type;
	}
	
	make() {
		if(this.type == 'android') {
			return new Android('Android Cake' , '5'); 
		}
	}
}


abstract class System {
	
	public name:string;
	public version: string;
	
	constructor(name:string , version:string) {
		this.name = name;
		this.version = version;
	}
	
	abstract isUpToDate() : boolean ;

	abstract currentVersion() : string;

	systemInfo() {
		return 'S.O : '+ this.name + ', Version : ' + this.version;
	}
}




class Android extends System {
	
	constructor(name:string , version:string) {
		super(name,version);
	}
	
	isUpToDate() {
		if(this.version == '7') {
			return true;
		}
		else {
			return false;
		}
	}
	
	currentVersion() {
		return this.version;
	}
}


let client1 = new Client('Vitor Mendes' , 19 , 'vitor.mendes@salaovip.com.br' , '431.666.378-00' , false);

let make = new Make('android');

let so = make.make();

let pedido1 = new Pedido(1 , client1);

console.log(so);