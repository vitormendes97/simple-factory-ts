var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Client = (function () {
    function Client(name, age, email, cpf, underage) {
        this.name = name;
        this.age = age;
        this.email = email;
        this.cpf = cpf;
        this.underage = underage;
    }
    Client.prototype.isUnderage = function () {
        if (this.underage) {
            return true;
        }
        else {
            return false;
        }
    };
    return Client;
}());
var Pedido = (function () {
    function Pedido(id, client) {
        this.id = id;
        this.client = client;
    }
    return Pedido;
}());
var Make = (function () {
    function Make(type) {
        this.type = type;
    }
    Make.prototype.make = function () {
        if (this.type == 'android') {
            return new Android('Android Cake', '5');
        }
    };
    return Make;
}());
var System = (function () {
    function System(name, version) {
        this.name = name;
        this.version = version;
    }
    System.prototype.systemInfo = function () {
        return 'S.O : ' + this.name + ', Version : ' + this.version;
    };
    return System;
}());
var Android = (function (_super) {
    __extends(Android, _super);
    function Android(name, version) {
        return _super.call(this, name, version) || this;
    }
    Android.prototype.isUpToDate = function () {
        if (this.version == '7') {
            return true;
        }
        else {
            return false;
        }
    };
    Android.prototype.currentVersion = function () {
        return this.version;
    };
    return Android;
}(System));
var client1 = new Client('Vitor Mendes', 19, 'vitor.mendes@salaovip.com.br', '431.666.378-00', false);
var make = new Make('android');
var so = make.make();
var pedido1 = new Pedido(1, client1);
console.log(so);
